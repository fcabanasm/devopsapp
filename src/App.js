import { useEffect, useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import axios from "axios";

function App() {
  const [insurance, setInsurance] = useState({});

  useEffect(() => {
    const fetchInsurance = async () => {
      const response = await axios.get(
        "https://dn8mlk7hdujby.cloudfront.net/interview/insurance/58"
      );
      const insurance = response?.data?.insurance;
      setInsurance(insurance);
    };

    fetchInsurance();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          {insurance.name} - ${insurance.price}
        </p>
        <img src={insurance.image} alt={insurance.name} />
        <p style={{ maxWidth: "40em" }}>{insurance.description}</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React =)
        </a>
      </header>
    </div>
  );
}

export default App;
