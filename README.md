
# Gitlab Pipeline for applications built with docker to deploy on Elastic Beanstalk

## IaC:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To try this pipeline you can set up all infrastructure by running pulumi up from this repo: [Pulumi](https://gitlab.com/fcabanasm/pulumi-beanstalk) 

---

## Stages:

### Test:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Basically it will run yarn test command that includes all test made by react testing library.

### Upload:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This stage will zip and upload this project to a s3 bucket defined at gitlab-ci.yml file as S3_BUCKET env.

### Deploy:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This last stage will create a new app version in Elastic Beanstalk and then will update its environment with that version, using the previus version uploaded to the s3 bucket.

---

## Environments:
- Dev: Will be trigger on every branch that start with 'feature'.
- Staging: Will be trigger on every merge request to master.
- Prod: Will be trigger on every commit to master branch.

---

Github Flow: 
![Github Flow](https://i2.wp.com/build5nines.com/wp-content/uploads/2018/01/GitHub-Flow.png "Github Flow")
